
Assume you have your source files a folder called `program_src`. You want to build the script called `main.py` and install `requirements.txt` from within the source folder. You will get the output in a folder called out.

```
docker container run --rm 
    -v "$(pwd)"/asset_control:/src 
    -v "$(pwd)"/out:/out 
    -e TMP_DIR=powercloud # Only set this if you really want the executable to use another temp dir than what the OS would suggest
    -e REQUIREMENTS=requirements_reduced.txt # Defaults to requirements.txt
    -e SCRIPT=run_client.py # Defaults to main.py
    pyinstaller

```
