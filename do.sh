. /venv/bin/activate

cd /src
pip install -r $REQUIREMENTS

TMP_DIR_CMD=""

if [[ -z "${TMP_DIR}" ]]; then
    TMP_DIR_CMD=""
else
    TMP_DIR_CMD="--runtime-tmpdir=$TMP_DIR"
fi

pyinstaller --onefile --paths=/venv/lib/python3.7/site-packages \
	    $TMP_DIR_CMD \
	    --specpath /out/spec \
	    --distpath /out/dist\
	    --workpath /out/build \
	    $SCRIPT

chown -R 1000:1000 /out/*
