FROM debian:jessie-slim

RUN apt-get update

RUN apt-get install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev \
                       libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev \
		       wget libffi-dev

RUN mkdir /openssl \
    && wget https://www.openssl.org/source/openssl-1.1.1c.tar.gz \
    && tar xzf openssl-1.1.1c.tar.gz \
    && cd openssl-1.1.1c \
    && ./config --prefix=/openssl \
    && make\
    && make install

ENV PV "3.8.1"
ENV PVSUB "3.8"

RUN echo $PV

RUN wget https://www.python.org/ftp/python/$PV/Python-$PV.tgz \
    && tar xzf Python-$PV.tgz \
    && cd Python-$PV \
    && ./configure --prefix=$HOME --enable-shared --with-openssl=/openssl LDFLAGS="-Wl,-rpath=/openssl/lib -L/openssl/lib" \
    && make \
    && make install


ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/Python-$PV/build/lib.linux-armv7l-$PVSUB:/root/lib
ENV PATH=$PATH:/root/bin

RUN /Python-$PV/python -m pip install virtualenv
RUN /Python-$PV/python -m virtualenv venv

RUN . /venv/bin/activate && pip install pyinstaller 

ENV REQUIREMENTS=requirements.txt
ENV SCRIPT=main.py

COPY do.sh .

CMD ["bash", "do.sh"]